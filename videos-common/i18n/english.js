[
    {
        "key":"_search_videos_",
        "value":"Search videos"
    },
    {
        "key":"_no_videos_",
        "value":"No Video"
    },
    {
        "key":"_search_videos_on_",
        "value":"Search videos on"
    },
    {
        "key":"_load_more_",
        "value":"Load More"
    },
    {
        "key":"_plays_",
        "value":"plays"
    },
    {
        "key":"_back_to_videos_",
        "value":"Back to videos"
    },
    {
        "key":"_favorites_",
        "value":"Favorites"
    },
    {
        "key":"_library_",
        "value":"Library"
    },
    {
        "key":"_playlists_",
        "value":"Playlists"
    },
    {
        "key":"_add_video_",
        "value":"Add video"
    },
    {
        "key":"_cancel_",
        "value":"Cancel"
    },
    {
        "key":"_my_uploads_",
        "value":"My Uploads"
    }
]