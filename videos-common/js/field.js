/**
 * Videos Plugin Assets
 *
 * @package   Videos Plugin
 * @author    Benjamin David
 * @copyright Copyright (c) 2013, Dukt
 * @license   http://dukt.net/
 * @link      http://dukt.net/
 */

// --------------------------------------------------------------------

// field plugin definition

(function($) {

    $.fn.videosField = function(options)
    {
        // build main options before element iteration
        // iterate and reformat each matched element

        return this.each(
            function()
            {
                field = $(this);

                $.fn.videosField.init(field);
            }
        );
    };

    // --------------------------------------------------------------------

    // init field

    $.fn.videosField.init = function(field)
    {
        videosApp.field.init(field);
    }

})(jQuery);
