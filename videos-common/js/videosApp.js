
// videos app

var initOnce = true;

var videosApp = {

	devMode: false,
	currentField: false,
	currentVideo:false,
	currentEmbedUrl:false,

	paginationMode: 'query', // 'query 'or 'search'
	paginationOptions: {},

	init : function() {

		videosApp.log('videosApp.init()');


		// center elements (no videos, videos placeholder, loading app)

		videosApp.utils.center($('.dkv-content.dkv-middle'), $('.dkv-no-videos'));
		videosApp.utils.center($('.dkv-content.dkv-middle'), $('.dkv-videos-placeholder'));
		videosApp.utils.center($('.dkv-app'), $('.dkv-loading-app'));


		// app init

		videosApp.resize();


		// run this only once

		if(initOnce) {

			videosApp.initOnce();

			initOnce = false;
		}
	},

	initOnce: function() {

		videosApp.devMode = Dukt_videos.devMode;

		$('.dkv-loading-app').removeClass('dkv-hidden');
		$('.dkv-app .container2').addClass('dkv-hidden');

		videosApp.player.init();
		videosApp.overlay.init();
		videosApp.scroll.init();


		// window resize

		$(window).resize(function() {
			videosApp.resize();
		});
	},

		log: function(msg) {

		// Add a prefix to each log

		// Not working because of console array like object arguments
		// arguments.splice(0, 0, "dukt_videos");

		// Thanks for your help in dealing with...

		// array-like objects
		// http://www.hacksparrow.com/javascript-array-like-objects.html

		// array splice and apply()
		// http://stackoverflow.com/questions/1348178/a-better-way-to-splice-an-arrray-into-an-array-in-javascript

		var slice_args = [0, 0].concat("dukt log : ");

		Array.prototype.splice.apply(arguments, slice_args);


		// apply arguments to the log

		if(typeof(videosApp.devMode) != 'undefined')
		{
		    if(console && videosApp.devMode){
		        console.debug.apply(console, arguments);
		    }
	    }

	},

    videoMore: {
        on: function() {
            $('.dkv-video-more').removeClass('dkv-hidden');
        },
        off: function() {
            $('.dkv-video-more').addClass('dkv-hidden');
        },
        loadingOn: function() {
            $('.dkv-video-more .dkv-btn').addClass('dkv-loading');
        },
        loadingOff: function() {
            $('.dkv-video-more .dkv-btn').removeClass('dkv-loading');
        },
        isLoading: function() {
            return $('.dkv-video-more .dkv-btn').hasClass('dkv-loading');
        }
    },

	search: {
		_timeout : false
	},

    addVideo: function() {

    	if(!$('.dkv-app .dkv-submit').hasClass('dkv-disabled')) {

	        videosApp.log('videosApp.field.addVideo()');

	        var video = videosApp.currentVideo;
	        var field = videosApp.currentField;

	        videosApp.field.embed(video.url, field);

	        $('input.text', field).attr('value', video.url);

	        videosApp.hide();
        }
    },

	cancel: function() {
		videosApp.hide();
	},

	show: function() {

		$('.dkv-app').css('display', 'block');

		videosApp.overlay.show();

		videosApp.resize();
	},

	hide: function() {

		// modal

		videosApp.overlay.hide();

		$('.dkv-app').css('display', 'none');

		videosApp.player.hide();
	},

	resize: function() {

		// player

		videosApp.player.resize();


		// modal

		videosApp.utils.center($(window), $('.dkv-app'));

		var contentH = $('.dkv-app').outerHeight() - ($('.dkv-top').outerHeight() + $('.dkv-bottom').outerHeight());

		$('.dkv-middle').css('height', contentH);

		videosApp.utils.center($('.dkv-content.dkv-middle'), $('.dkv-videos-placeholder'));


		// thumbnails (with hd ratio)

		var hd = {w:1280, h:720};

		var videoImgW = $('.dkv-video-img').outerWidth();
		var videoImgH = Math.round(videoImgW * hd.h / hd.w);

		$('.dkv-videos-thumb .dkv-clip-outer img').css('width', videoImgW);
		$('.dkv-videos-thumb .dkv-clip-outer').css('width', videoImgW);
		$('.dkv-videos-thumb .dkv-clip-outer').css('height', videoImgH);


		// play

		videosApp.utils.center($('.dkv-videos-thumb'), $('.dkv-play'));


		// no videos

		videosApp.utils.center($('.dkv-content.dkv-middle'), $('.dkv-no-videos'));


		// placeholder

		videosApp.utils.center($('.dkv-content.dkv-middle'), $('.dkv-videos-placeholder'));

		setTimeout(function() {
			videosApp.scroll.height = $('.dkv-videos').outerHeight();

			videosApp.scroll.init();
		}, 1);
	},

	utils: {

		// center 'element' to 'container'

		center: function(container, element) {

			var top = Math.round((container.outerHeight() - element.outerHeight()) / 2);
			var left = Math.round((container.outerWidth() - element.outerWidth()) / 2);

			element.css('top', top);
			element.css('left', left);
		}
	}
};

// --------------------------------------------------------------------

// loading

videosApp.loading = {

	on: function() {
		videosApp.log('videosApp.loading.on()');

		$('.dkv-videos-placeholder').removeClass('dkv-hidden');

		$('.dkv-no-videos').addClass('dkv-hidden');

		$('.dkv-reload').addClass('dkv-loading');
	},

	off: function() {
		videosApp.log('videosApp.loading.off()');

		$('.dkv-reload').removeClass('dkv-loading');
		$('.dkv-videos-placeholder').addClass('dkv-hidden');
		$('.dkv-no-videos').removeClass('dkv-hidden');

		videosApp.videoMore.loadingOff();
	}
};

// --------------------------------------------------------------------

// overlay

videosApp.overlay = {
	init:function() {

		videosApp.log('videosApp.overlay.init()');

		$('.dkv-overlay').appendTo('body');

		$('.dkv-overlay').click(function() {

			if(!$('.dkv-player').hasClass('dkv-hidden')) {
				// only close the player

				videosApp.player.hide();

			} else {

				// close everything

				videosApp.hide();
			}
		})
	},

	show: function() {
		videosApp.log('videosApp.overlay.show()');

		$('.dkv-overlay').removeClass('hidden');
	},

	hide: function() {

		videosApp.log('videosApp.overlay.hide()');

		$('.dkv-overlay').addClass('hidden');
	}
};

// --------------------------------------------------------------------

// scroll

var scrollCount = 0;

videosApp.scroll = {

	height:0,

	init: function() {

		var container = $('.dkv-content.dkv-middle');

		container.unbind('scroll');

		container.scroll(function () {


			videosApp.log('scroll', ($('.dkv-content.dkv-middle').scrollTop() + $('.dkv-content.dkv-middle').height()), videosApp.scroll.height);

			if ($('.dkv-content.dkv-middle').scrollTop() + $('.dkv-content.dkv-middle').height() >= videosApp.scroll.height) {
					videosApp.scroll.trigger();
			}
		});
	},

	check: function(scrollWrap, scrollContent) {

		// the offset triggers the load more earlier

		var scrollOffset = scrollWrap.outerHeight() * 2;

		scrollWrap.scroll(function () {

			// videosApp.log(
			//     'scroll',
			//     scrollWrap.scrollTop(),
			//     scrollWrap.height(),
			//     scrollOffset,
			//     (scrollWrap.scrollTop() + scrollWrap.height() + scrollOffset),
			//     scrollContent.height()
			// );

			if ((scrollWrap.scrollTop() + scrollWrap.height() + scrollOffset) >= scrollContent.height()) {
					videosApp.scroll.trigger();
			}
		});
	},

	trigger: function() {

		// Works perfect for desktop browsers

		if($('.dkv-video-more').css('display') != "none") {
				videosApp.log('videosApp.trigger()');
				$('.dkv-video-more a').trigger('click');
		}
	}
};