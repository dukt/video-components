// player

videosApp.player = {

	isModal: false,
	video: false,

	init: function() {

		// move player to body

		// $('.dkv-player').appendTo('body');

	},

	show: function() {
		videosApp.log('videosApp.player.show()');

		$('.dkv-player iframe').attr('src', videosApp.currentEmbedUrl);

		videosApp.overlay.show();

		$('.dkv-player').removeClass('dkv-hidden');
	},

	hide:function() {

		videosApp.log('videosApp.player.hide()');

		$('.dkv-player iframe').attr('src', '');

		// videosApp.overlay.hide();

		$('.dkv-player').addClass('dkv-hidden');
	},

	resize: function() {

		var el = $('.dkv-player');


		var top = ($(window).outerHeight() - el.outerHeight()) / 2;
		var left = ($(window).outerWidth() - el.outerWidth()) / 2;

		el.css('top', top);
		el.css('left', left);

		var width = $('.dkv-app').outerWidth();
		var height = $('.dkv-app').outerHeight();

		el.css('width', width);
		el.css('height', height);

		var iframeHeight = height - $('.dkv-bottom', el).outerHeight();

		$('iframe', el).css('height', iframeHeight);
	}
};