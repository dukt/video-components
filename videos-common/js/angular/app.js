'use strict';

/* App Module */

angular.module('videosapp', ['localization', 'videoService', 'videoProvidersService']).


	// prevent scroll to top of page

	value('$anchorScroll', angular.noop).


	// routes

	config(['$routeProvider', function($routeProvider, $rootScope) {

		var mainPartial = DuktVideosCms.getResourceUrl('components/videos-common/partials/main.html');

		$routeProvider.
		when('/', {templateUrl: mainPartial,   controller: MainCtrl}).
		when('/:segment1', {templateUrl: mainPartial,   controller: MainCtrl}).
		when('/:segment1/:segment2', {templateUrl: mainPartial,   controller: MainCtrl}).
		when('/:segment1/:segment2/:segment3', {templateUrl: mainPartial,   controller: MainCtrl}).
		when('/:segment1/:segment2/:segment3/:segment4', {templateUrl: mainPartial,   controller: MainCtrl}).
		otherwise({redirectTo: '/'});

	}]);