'use strict';

/* Controllers */

function MainCtrl($scope, $rootScope, $route, $routeParams, $location, Video, VideoProviders) {

	$scope.init = function() {

		videosApp.init();

		// segments

		$scope.segment1 = $routeParams.segment1;
		$scope.segment2 = $routeParams.segment2;
		$scope.segment3 = $routeParams.segment3;


		// load providers

		if(!$rootScope.videoProviders) {

			VideoProviders.all({}, function(data) {

				$('.dkv-loading-app').addClass('dkv-hidden');
				$('.dkv-app .container2').removeClass('dkv-hidden');

				$rootScope.videoProviders = data.result;



				if($scope.segment1) {


					// define current provider

					$.each($rootScope.videoProviders, function(a, b) {

						if(this.handle == $scope.segment1) {
							$rootScope.currentProvider = this;

							videosApp.log('current provider : ', $rootScope.currentProvider);
						}
					});
				}

				$scope.providerRouting();

				videosApp.resize();
			});
		} else {
			videosApp.log('providers are already defined');

			$scope.providerRouting();
		}


		// search autofocus

		if($scope.segment2 == 'search') {
			$('.dkv-search input').trigger('focus');
			setTimeout(function() {
					$scope.search();
			}, 10);
		}
	}


	// search

	$scope.search = function() {

		// videosApp.log('MainCtrl : $scope.search()');

		videosApp.loading.on();

		if(this.searchQuery == "") {

			$rootScope.searchQuery = this.searchQuery;

			$route.reload();

		} else if(this.searchQuery != $rootScope.searchQuery) {

			$rootScope.searchQuery = this.searchQuery;

			clearTimeout(videosApp.search._timeout);

			videosApp.search._timeout = setTimeout(function() {
				videosApp.log('start searching');

				videosApp.loading.off();

				if($rootScope.searchQuery != "") {
					videosApp.loading.on();


					videosApp.log('search query start');

					$scope._searchRequest();
				}
			}, 1000);
		}
	};


	// searchRequest

	$scope._searchRequest = function(options, addResult) {

		videosApp.paginationMode = 'search';

		videosApp.paginationOptions =  {
			method:'search',
			source:$scope.segment1,
			searchQuery: $rootScope.searchQuery,
			page:1,
			perPage: Dukt_videos.pagination_per_page
		};

		videosApp.paginationOptions = $.extend({}, videosApp.paginationOptions, options);

		videosApp.log('-----options', videosApp.paginationOptions);

		videosApp.loading.on();

		Video.search(videosApp.paginationOptions, function(data) {
			videosApp.log('search query end');
			videosApp.loading.off();

			$('.dkv-app .dkv-submit').addClass('dkv-disabled');

			if(typeof(data.result) != 'undefined') {
				videosApp.loading.on();

				videosApp.log('- getVideos', data);

				if(addResult) {
					$scope.videos = $.merge($scope.videos, data.result);
				} else {
					$scope.videos = data.result;
				}

				videosApp.log('- getVideos length', $scope.videos.length);

				setTimeout(function() {
					videosApp.scroll.height = $('.dkv-videos').outerHeight();
				}, 1);

				// post get videos...

				videosApp.loading.off();

			} else {
				$scope.videos = [];
			}


			// no videos ?

			if($scope.videos.length > 0) {
				$('.dkv-no-videos').addClass('dkv-hidden');

                if(data.result.length < Dukt_videos.pagination_per_page) {
                    videosApp.videoMore.off();
                } else {
                    videosApp.videoMore.on();
                }

			} else {
				$('.dkv-videos-placeholder').addClass('dkv-hidden');
				$('.dkv-no-videos').removeClass('dkv-hidden');
			}

			videosApp.log('stop searching');


			videosApp.resize();

			setTimeout(function() {
				videosApp.resize();
			}, 1);
		});
	}


	// providerRouting

	$scope.providerRouting = function() {

		videosApp.log('MainCtrl.providerRouting()');

		if(!$scope.segment1) {
			videosApp.log('- segment1 not found');

			var i = 0;
			angular.forEach($rootScope.videoProviders, function(provider, key){

				if(i == 0) {
					videosApp.log('- redirect to : '+ provider['sections'][0].childs[0]['url']);
				   	$location.path(provider['sections'][0].childs[0]['url']);
				}

				i++;
			});
		}

		if(!$scope.segment2) {
			videosApp.log('- segment2 not found');

			if($rootScope.currentProvider) {
				angular.forEach($rootScope.videoProviders, function(provider, key){

					if($routeParams.segment1 == provider.handle) {
						videosApp.log('redirect to : '+ provider['sections'][0].childs[0]['url']);
					   	$location.path(provider['sections'][0].childs[0]['url']);
					}
					i++;
				});
			}
		}


		// define current provider
		videosApp.log($rootScope.videoProviders);
		$.each($rootScope.videoProviders, function(a, b) {
			if(this.handle == $scope.segment1) {
				$rootScope.currentProvider = this;
				videosApp.log('- define current provider : ', $rootScope.currentProvider);
			}
		});


		// get videos

		videosApp.loading.on();


		$scope._videoQueryRequest();
	};


	// videoQueryRequest

	$scope._videoQueryRequest = function(options, addResult) {

		videosApp.paginationMode = 'query';

		videosApp.paginationOptions = {
			method: 'videos',
			path: $location.path(),
			page:1,
			perPage: Dukt_videos.pagination_per_page
		};

		videosApp.paginationOptions = $.extend({}, videosApp.paginationOptions, options);

		Video.query(videosApp.paginationOptions, function(data) {

			videosApp.loading.on();

			videosApp.log('- getVideos', data);

			if(addResult) {
				$scope.videos = $.merge($scope.videos, data.result);
			} else {
				$scope.videos = data.result;
			}

			videosApp.log('- getVideos length', $scope.videos.length);

			setTimeout(function() {
				videosApp.scroll.height = $('.dkv-videos').outerHeight();
			}, 1);

			// post get videos...

			videosApp.loading.off();

			// no videos ?

			if($scope.videos.length > 0) {
				$('.dkv-no-videos').addClass('dkv-hidden');

                if(data.result.length < Dukt_videos.pagination_per_page) {
                    videosApp.videoMore.off();
                } else {
                    videosApp.videoMore.on();
                }

			} else {
				$('.dkv-videos-placeholder').addClass('dkv-hidden');
				$('.dkv-no-videos').removeClass('dkv-hidden');
			}

			videosApp.resize();

			setTimeout(function() {
				videosApp.resize();
			}, 1);
		});
	}



	// sidebar active

	$scope.getClass = function(path) {
		// videosApp.log('compare paths', $location.path().substr(0, path.length), path);
	    if ($location.path().substr(0, path.length) == path) {
	      return "active"
	    } else {
	      return ""
	    }
	}


	// add video

	$scope.addVideo = function()
	{
		videosApp.log('$scope.addVideo()');

		videosApp.addVideo();
	}


	// cancel

	$scope.cancel = function()
	{
		videosApp.cancel();
	}


	// back to videos

	$scope.back = function()
	{
		videosApp.log('$scope.back()');

		videosApp.player.hide();
	}


	// select a video

    $scope.select = function(video)
    {
        $scope.selected = videosApp.currentVideo = video;

        $('.dkv-app .dkv-submit').removeClass('dkv-disabled');
    }


	// play

    $scope.play = function(video)
    {
        videosApp.log('play a video : ', video);

		var videoDetails = Video.post({
			method:'embedUrl',
			source:$scope.segment1,
			videoUrl: video.url,
			embedOptions: {
				autoplay: 1
			}
		}, function(data) {

			videosApp.log('embedUrl', data);

			videosApp.currentEmbedUrl = data.embedUrl;

			videosApp.player.show();

			videosApp.resize();
		});

    }


    // isSelected

    $scope.isSelected = function(video) {
        return $scope.selected === video;
    }


    // more videos

	$scope.moreVideos = function()
	{
        if(!videosApp.videoMore.isLoading()) {

            videosApp.videoMore.loadingOn();

    		var perPage = Dukt_videos.pagination_per_page;

    		var offset = $scope.videos.length;

    		var page = Math.floor(offset / perPage) + 1;

    		var opts = {
    			page:page,
    			perPage:perPage
    		};

    		$scope._performRequest(opts);
        }
	}


	// perform request

	$scope._performRequest = function(opts) {
		// videosApp.paginationMode

		var mergedOpts = $.extend({}, videosApp.paginationOptions, opts);

		videosApp.log('_performRequest', videosApp.paginationMode, videosApp.paginationOptions, opts, mergedOpts);

		switch(videosApp.paginationMode) {
			case 'search':
				$scope._searchRequest(mergedOpts, true);
				break;

			case 'query':
				$scope._videoQueryRequest(mergedOpts, true);
				break;
		}
	}


	// init

	$scope.init();


    // press enter triggers search

    $(document).on('keypress', '.dkv-search input', function(e) {
    	videosApp.log('enter keypress');
        if(e.keyCode == "13") {
            $scope._searchRequest();
        }
    });
}
