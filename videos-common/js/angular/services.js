'use strict';

/* Services */

angular.module('videoService', ['ngResource']).
factory('Video', function($resource){
	return $resource(Dukt_videos.endpointUrl+'&method=:method&source=:source&path=:path&videoId=:videoId&page=:page&perPage=:perPage', {}, {
		query: {method:'GET', params:{method:'@method', path:'@path', videoId:'@videoId', source:'@source', page:'@page', perPage:'@perPage'}},
		post: {method:'POST', params:{method:'@method', path:'@path', videoId:'@videoId', source:'@source', page:'@page', perPage:'@perPage'}},
		search: {method:'POST', isArray: false, params:{method:'@method', source:'@source', page:'@page', perPage:'@perPage'}},
	});
});

angular.module('videoProvidersService', ['ngResource']).
factory('VideoProviders', function($resource){
	return $resource(Dukt_videos.endpointUrl+'&method=sources', {}, {
		all: {method:'GET'}
	});
});