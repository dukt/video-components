// field

videosApp.field = {

    init: function(field) {

        videosApp.log('videosApp.field.init()');


        // if a video is already set, load the iframe

        var videoUrl = $('input.text', field).val();

        if(videoUrl !== "") {

            // a video is set

            videosApp.field.embed(videoUrl, field);
        } else {
            $('.dkv-add', field).css('display', 'inline-block');
            $('.dkv-change', field).css('display', 'none');
            $('.dkv-remove', field).css('display', 'none');
        }


        // btns

        $('.dkv-add, .dkv-change', field).click(function() {
	        videosApp.currentField = field;
	        videosApp.show();
        });

        $('.dkv-remove', field).click(function() {
            videosApp.field.removeVideo(field);
        });
    },

    embed: function(videoUrl, field) {

        videosApp.log('videosApp.field.embed()');


        // request field preview embed

        DuktVideosCms.postActionRequest('embed', {videoUrl:videoUrl}, function(response) {

            videosApp.log('videosApp.field.embed() : ajax : embed', response);


            // load modal body

            var fieldPreview = $('.dkv-embed', field);

            fieldPreview.html('');
            fieldPreview.css('display', 'block');

            $(response['embed']).appendTo(fieldPreview);

            $('.dkv-add', field).css('display', 'none');
            $('.dkv-change', field).css('display', 'inline-block');
            $('.dkv-remove', field).css('display', 'inline-block');


            // resize

            videosApp.field.resize();
        });
    },

    removeVideo: function(field) {

        videosApp.log('videosApp.field.removeVideo()');

        // input

        $('input.text', field).val('');


        // embed

        $('.dkv-embed', field).css('display', 'none');
        $('.dkv-embed', field).html();


        // buttons

        $('.dkv-add', field).css('display', 'inline-block');
        $('.dkv-change', field).css('display', 'none');
        $('.dkv-remove', field).css('display', 'none');
    },

    resize: function() {

        videosApp.log('videosApp.field.resize()');

        var hdSize = {w:1280,h:720};
        var fieldW = $('.dkv-field').outerWidth();
        var embedH = fieldW * hdSize.h / hdSize.w;

        $('.dkv-field .dkv-embed').css('height', embedH);
    }
};